//www.elegoo.com
//2016.12.9 

#include <Servo.h>

#define SERVO_PIN 6

Servo motor;

void setup() 
{
    Serial.begin(9600);

    motor.attach(SERVO_PIN);

    motor.write(0);
}

void loop()
{
    delay(5000);

    // Max
    Serial.println("mouvement 180");
    motor.write(180);

    delay(5000);

    // moitier
    Serial.println("mouvement 90");
    motor.write(90);

    delay(5000);

    // revient à 0
    Serial.println("mouvement 0");
    motor.write(0);
}

