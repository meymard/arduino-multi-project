//www.elegoo.com
//2016.12.9

#include <Servo.h>

#define LATCH_PIN 11    // (11) ST_CP [RCK] on 74HC595
#define CLOCK_PIN 9     // (9) SH_CP [SCK] on 74HC595
#define DATA_PIN 12     // (12) DS [S1] on 74HC595

#define BTN_LED_1 A0
#define BTN_LED_2 A1
#define BTN_LED_3 A2
#define BTN_LED_4 A3
#define BTN_VALIDER A4

#define SERVO_PIN 3

int wait = 1000;

byte leds[2];
byte leds_registre1 = 0;
byte leds_registre2 = 0;

bool vals_possibles[8][3] = {
    {false, false, false},  // eteint       0
    {true, false, false},   // rouge        1
    {false, true, false},   // vert         2
    {false, false, true},   // bleu         3
    {true, true, false},    // jaune        4
    {true, true, true},     // blanc        5
    {false, true, true},    // bleu clair   6
    {true, false, true},    // rose         7
};

int val_led1 = 0;
int val_led2 = 0;
int val_led3 = 0;
int val_led4 = 0;

int led1_win = 2;
int led2_win = 1;
int led3_win = 7;
int led4_win = 5;

Servo motor;

void setup()
{
    leds[0] = 0;
    leds[1] = 0;
    Serial.begin(9600);

    pinMode(LATCH_PIN, OUTPUT);
    pinMode(DATA_PIN, OUTPUT);
    pinMode(CLOCK_PIN, OUTPUT);

    pinMode(BTN_LED_1, INPUT_PULLUP);
    pinMode(BTN_LED_2, INPUT_PULLUP);
    pinMode(BTN_LED_3, INPUT_PULLUP);
    pinMode(BTN_LED_4, INPUT_PULLUP);
    pinMode(BTN_VALIDER, INPUT_PULLUP);

    motor.attach(SERVO_PIN);

    init_leds();

    motor.write(0);
}

void loop()
{
    int led = 0;
    int *val_led = -1;


    if (digitalRead(BTN_VALIDER) == LOW)
    {
        if (valider()) {
            motor.write(90);
            delay(3000);
        } else {
            motor.write(0);
        }
        init_leds();

        return;
    }

    if (digitalRead(BTN_LED_1) == LOW)
    {
        led = 1;
        val_led = &val_led1;
    }
    if (digitalRead(BTN_LED_2) == LOW)
    {
        led = 2;
        val_led = &val_led2;
    }
    if (digitalRead(BTN_LED_3) == LOW)
    {
        led = 3;
        val_led = &val_led3;
    }
    if (digitalRead(BTN_LED_4) == LOW)
    {
        led = 4;
        val_led = &val_led4;
    }

    if (led > 0) {
        *val_led = *val_led + 1;
        if (*val_led >= 8) {
            *val_led = 0;
        }

        int vled = *val_led;
        change_color(led, vals_possibles[vled][0], vals_possibles[vled][1], vals_possibles[vled][2]);

        Serial.print("Val led : ");
        Serial.println(*val_led);
        delay(300);
    }
}

void change_color(int led, bool r, bool g, bool b)
{
    int bit_r, bit_g, bit_b;

    byte* registre;

    switch (led) {
        case 1:
            bit_r = 7;
            bit_g = 6;
            bit_b = 5;
            registre = &leds[1];
            break;
        case 2:
            bit_r = 4;
            bit_g = 3;
            bit_b = 2;
            registre = &leds[1];
            break;
        case 3:
            bit_r = 7;
            bit_g = 6;
            bit_b = 5;
            registre = &leds[0];
            break;
        case 4:
            bit_r = 4;
            bit_g = 3;
            bit_b = 2;
            registre = &leds[0];
            break;
        default:
            return;
    }
    Serial.print("Led ");
    Serial.println(led);

    Serial.print("val ");
    if (r) {
        Serial.print("r");
    }
    if (g) {
        Serial.print("g");
    }
    if (b) {
        Serial.print("b");
    }
    Serial.println("");

    bitWrite(*registre, bit_r, (int) r);
    bitWrite(*registre, bit_g, (int) g);
    bitWrite(*registre, bit_b, (int) b);

        //bitWrite(leds[0], 1, 1);
        //bitWrite(leds[1], 6, 1);


    digitalWrite(LATCH_PIN, LOW);
    shiftOut(DATA_PIN, CLOCK_PIN, LSBFIRST, leds[0]);
    shiftOut(DATA_PIN, CLOCK_PIN, LSBFIRST, leds[1]);
    digitalWrite(LATCH_PIN, HIGH);
    //digitalWrite(LATCH_PIN, LOW);
    //shiftOut(DATA_PIN, CLOCK_PIN, LSBFIRST, leds[1]);
    //digitalWrite(LATCH_PIN, HIGH);
}

bool valider()
{
    return
        val_led1 == led1_win
        && val_led2 == led2_win
        && val_led3 == led3_win
        && val_led4 == led4_win
    ;
}

void init_leds()
{
    Serial.print("Led 1, val : ");
    Serial.print(val_led1);
    Serial.print(", attendu : ");
    Serial.println(led1_win);
    Serial.print("Led 2, val : ");
    Serial.print(val_led2);
    Serial.print(", attendu : ");
    Serial.println(led2_win);
    Serial.print("Led 3, val : ");
    Serial.print(val_led3);
    Serial.print(", attendu : ");
    Serial.println(led3_win);
    Serial.print("Led 4, val : ");
    Serial.print(val_led4);
    Serial.print(", attendu : ");
    Serial.println(led4_win);

    val_led1 = 0;
    val_led2 = 0;
    val_led3 = 0;
    val_led4 = 0;
    for (int i = 1; i <= 4; i++) {
        change_color(i, false, false, false);
    }
}
