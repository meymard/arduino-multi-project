//www.elegoo.com
//2016.12.9

#include <LiquidCrystal.h>
#include <Servo.h>

#define SW_PIN 2 // digital pin connected to switch output
#define X_PIN A1 // analog pin connected to X output
#define Y_PIN A0 // analog pin connected to Y output

#define SERVO_PIN 6


Servo motor;

const int motor_ferme = 180;
const int motor_ouvert = 90;
const int max_pos = 16;

const char code[max_pos] = {'p', 'a', 'p', 'i', 'l', 'l', 'o', 't', 'e'};

int position = 0;
int lettreMvt = 0;
char selected[max_pos];

bool win = false;

//                BS  E  D4 D5  D6 D7
LiquidCrystal lcd(7, 8, 9, 10, 11, 12);
void setup()
{
    pinMode(SW_PIN, INPUT);
    digitalWrite(SW_PIN, HIGH);
    Serial.begin(9600);

    motor.attach(SERVO_PIN);

    motor.write(motor_ferme);

    Serial.begin(9600);

    lcd.begin(16, 2);

    clearLines();
}

void loop()
{
    if (win) {
        if (digitalRead(SW_PIN) == LOW) {
            reset();
            motor.write(motor_ferme);
            win = false;
            delay(1000);
        }
        return;
    }

    int oldPos = position;
    int oldLettreMvt = lettreMvt;

    Serial.print("btn ");
    Serial.println(digitalRead(SW_PIN));
    Serial.print("x  ");
    Serial.println(analogRead(X_PIN));
    Serial.print("y  ");
    Serial.println(analogRead(Y_PIN));

    getButtonsInfo();
    showText();

    if (digitalRead(SW_PIN) == LOW) {
        bool is_ok = validateCode();
        clearLines();

        if (is_ok) {
            lcd.setCursor(0, 0);
            lcd.print("Bravo !");
            lcd.setCursor(0, 1);
            lcd.print("Cool !");
            motor.write(motor_ouvert);

            win = true;
            delay(1000);
        } else {
            lcd.setCursor(0, 0);
            lcd.print("Dommage !");
            lcd.setCursor(0, 1);
            lcd.print("Tu peux reessayer.");
            motor.write(motor_ferme);
            delay(5000);

            reset();
        }
    }

    if (
        oldPos != position
        || lettreMvt != 0
    ) {
        delay(300);
    }
}

void reset() {
    for (int i = 0; i < max_pos; i++) {
        selected[i] = NULL;
    }

    position = 0;

    clearLines();
}

bool validateCode() {
    for (int i = 0; i < max_pos; i++) {
        if (selected[i] != code[i]) {
            return false;
        }
    }

    return true;
}

void showText() {
    if (position-1 >= 0) {
        lcd.setCursor(position-1, 1);
        lcd.print(" ");
    }
    if (position+1 < max_pos) {
        lcd.setCursor(position+1, 1);
        lcd.print(" ");
    }
    lcd.setCursor(position, 1);
    lcd.print("^");

    char lettre = getChar(position, lettreMvt);
    lcd.setCursor(position, 0);
    if (lettre == NULL) {
        lcd.print(" ");
    } else {
        lcd.print(lettre);
    }
}

void printSolution() {
    for (int i = 0; i < max_pos; i++) {
        lcd.setCursor(i, 0);
        lcd.print(code[i]);
    }
}

char getChar(int pos, int mvt) {
    char previous = selected[pos];
    char newVal = previous;
    if (mvt == -1) {
        if (previous == 'a') {
            newVal = NULL;
        } else if (previous == NULL) {
            newVal = 'z';
        } else {
            newVal = (int)previous - 1;
        }
    } else if (mvt == 1) {
        if (previous == 'z') {
            newVal = NULL;
        } else if (previous == NULL) {
            newVal = 'a';
        } else {
            newVal = (int)previous + 1;
        }
    }
    selected[pos] = newVal;

    if (newVal != previous) {
        Serial.print("Lettre ");
        Serial.print(newVal);
        Serial.print(", mvt ");
        Serial.println(mvt);
    }

    return newVal;
}

void clearLine(int l) {
    lcd.setCursor(0, l);
    for (int j = 0;j < max_pos;j++) {
        lcd.print(" ");
    }
}

void clearLines() {
    lcd.setCursor(0, 0);
    clearLine(0);
    clearLine(1);
}

void getButtonsInfo() {
    int oldPos = position;

    int x = analogRead(X_PIN);
    int y = analogRead(Y_PIN);

    if (x > 600) {
        position -= 1;
        if (position < 0) {
            position = 0;
        }
    } else if (x < 400) {
        position += 1;
        if (position >= max_pos) {
            position = max_pos - 1;
        }
    }
    if (oldPos != position) {
        Serial.print("Position  ");
        Serial.println(position);
    }

    int oldLettreMvt = lettreMvt;

    if (y < 400) {
        lettreMvt = -1;
    } else if (y > 600) {
        lettreMvt = 1;
    } else {
        lettreMvt = 0;
    }
    if (oldLettreMvt != lettreMvt) {
        Serial.print("Lettre mvt  ");
        Serial.println(lettreMvt);
    }
}
