//www.elegoo.com
//2016.12.08

#define BLUE 3
#define GREEN 5
#define RED 6

#define BUTTON_BLUE 4
#define BUTTON_GREEN 7
#define BUTTON_RED 8

int blueVal = 0;
int greenVal = 0;
int redVal = 0;

void setup() 
{
    pinMode(RED, OUTPUT);
    pinMode(GREEN, OUTPUT);
    pinMode(BLUE, OUTPUT);
    digitalWrite(RED, LOW);
    digitalWrite(GREEN, LOW);
    digitalWrite(BLUE, LOW);

    pinMode(BUTTON_RED, INPUT_PULLUP);
    pinMode(BUTTON_GREEN, INPUT_PULLUP);
    pinMode(BUTTON_BLUE, INPUT_PULLUP);

        analogWrite(RED, redVal);
        analogWrite(BLUE, blueVal);
        analogWrite(GREEN, greenVal);
}

void updateVal(int *v)
{
    *v += 50;
    if (*v > 255) {
        *v = 0;
    }
}

void loop()
{
    bool update = false;
    if (digitalRead(BUTTON_BLUE) == LOW)
    {
        updateVal(&blueVal);
        update = true;
    }
    if (digitalRead(BUTTON_GREEN) == LOW)
    {
        updateVal(&greenVal);
        update = true;
    }
    if (digitalRead(BUTTON_RED) == LOW)
    {
        updateVal(&redVal);
        update = true;
    }
    if (update) {
        analogWrite(RED, redVal);
        analogWrite(BLUE, blueVal);
        analogWrite(GREEN, greenVal);
        delay(100);
    }
}
