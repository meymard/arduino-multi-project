int latchPin = 11;
int clockPin = 9;
int dataPin = 12;

int numOfRegisters = 2;
byte* registerState;

long effectId = 0;
long prevEffect = 0;
long effectRepeat = 0;
long effectSpeed = 30;

void setup() {
	//Initialize array
	registerState = new byte[numOfRegisters];
	for (size_t i = 0; i < numOfRegisters; i++) {
		registerState[i] = 0;
	}

	//set pins to output so you can control the shift register
	pinMode(latchPin, OUTPUT);
	pinMode(clockPin, OUTPUT);
	pinMode(dataPin, OUTPUT);

    Serial.begin(9600);
}

void loop() {
    for (int i = 15; i >= 0; i--){
        regWrite(i, LOW);
    }
    for (int i = 15; i >= 0; i--){
        Serial.print("i : ");
        Serial.println(i);
        for (int k = 0; k < i; k++){
            Serial.print("k : ");
            Serial.println(k);
            regWrite(k, HIGH);
            delay(500);
            regWrite(k, LOW);
        }

        regWrite(i, HIGH);
            delay(500);
    }
    return;

    int r = 7;
    int g = 6;
    int b = 5;
    regWrite(r, 1);
    delay(500);
    regWrite(g, 1);
    delay(500);
    regWrite(b, 1);
    delay(500);
    regWrite(r, 0);
    delay(500);
    regWrite(g, 0);
    delay(500);
    regWrite(b, 0);
    delay(500);
    regWrite(g, 1);
    delay(500);
    regWrite(b, 1);
    delay(500);
    //for (int i = 0; i <= 16; i++) {
    //    regWrite(i, 1);
    //    delay(500);
    //}
    //for (int i = 0; i <= 16; i++) {
    //    regWrite(i, 0);
    //    delay(500);
    //}
    //for (int i = 0; i <= 16; i = i+ 2) {
    //    regWrite(i, 0);
    //    delay(500);
    //}
    //for (int i = 0; i <= 16; i++) {
    //    regWrite(i, 0);
    //    delay(500);
    //}
}

void regWrite(int pin, bool state){
	//Determines register
	int reg = pin / 8;
	//Determines pin for actual register
	int actualPin = pin - (8 * reg);

    Serial.print("actual pin : ");
    Serial.println(actualPin);

	//Begin session
	digitalWrite(latchPin, LOW);

	for (int i = 0; i < numOfRegisters; i++){
		//Get actual states for register
		byte* states = &registerState[i];

		//Update state
		if (i == reg){
			bitWrite(*states, actualPin, state);
		}

		//Write
		shiftOut(dataPin, clockPin, MSBFIRST, *states);
	}

	//End session
	digitalWrite(latchPin, HIGH);
}
