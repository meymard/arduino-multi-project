#include "harry_potter.h"

HarryPotter::HarryPotter() {
}

int *HarryPotter::melody() {
    return this->notes;
}

int *HarryPotter::tempo() {
    int t[62] = {
        2, 4,
        4, 8, 4,
        2, 4,
        2, 
        2,
        4, 8, 4,
        2, 4,
        1, 
        4,

        4, 8, 4,
        2, 4,
        2, 4,
        2, 4,
        4, 8, 4,
        2, 4,
        1,
        4,

        2, 4,
        2, 4,
        2, 4,
        2, 4,
        4, 8, 4,
        2, 4,
        1, 
        4, 4,  

        2, 4,
        2, 4,
        2, 4,
        2, 4,
        4, 8, 4,
        2, 4,
        1
    };

    return t;
}
