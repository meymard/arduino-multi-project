//www.elegoo.com
//2016.12.9 

#include <Servo.h>

#define GREEN_1 7
#define RED_1 6
#define BLUE_1 5
#define GREEN_2 4
#define RED_2 3
#define BLUE_2 2
#define GREEN_3 1
#define RED_3 0
#define BLUE_3 9

#define BUTTON_1 4
#define BUTTON_2 7
#define BUTTON_3 8

#define LED_NONE 0
#define LED_GREEN 1
#define LED_RED 2
#define LED_BLUE 3

#define SERVO_PIN 3

int tDelay = 100;
int latchPin = 11;      // (11) ST_CP [RCK] on 74HC595
int clockPin = 9;      // (9) SH_CP [SCK] on 74HC595
int dataPin = 12;     // (12) DS [S1] on 74HC595

byte leds = 0;

int state1 = 0;
int state2 = 0;
int state3 = 0;

bool win = false;

Servo motor;

void updateShiftRegister()
{
    Serial.print("leds : ");
    Serial.println(leds);
   digitalWrite(latchPin, LOW);
   shiftOut(dataPin, clockPin, LSBFIRST, leds);
   digitalWrite(latchPin, HIGH);
}

void setup() 
{
    Serial.begin(9600);

    pinMode(BUTTON_1, INPUT_PULLUP);
    pinMode(BUTTON_2, INPUT_PULLUP);
    pinMode(BUTTON_3, INPUT_PULLUP);

    pinMode(latchPin, OUTPUT);
    pinMode(dataPin, OUTPUT);  
    pinMode(clockPin, OUTPUT);

    motor.attach(SERVO_PIN);

    leds = 0;
    updateShiftRegister();
    delay(tDelay);
    for (int i = 0; i < 8; i++)
    {
        bitSet(leds, i);
        updateShiftRegister();
        delay(tDelay);
    }

    motor.write(0);

    leds = 0;
    updateShiftRegister();
    delay(tDelay);
}

int updateState(int state, int green, int blue, int red)
{
    Serial.print("state avant : ");
    Serial.println((int) state);
    state += 1;
    Serial.print("state après : ");
    Serial.println((int) state);
    if (state > 3) {
        state = 0;
    Serial.print("state après réinistalisé : ");
    Serial.println((int) state);
    }
    switch (state) {
        case LED_NONE:
            bitClear(leds, green);
            bitClear(leds, blue);
            bitClear(leds, red);
            break;
        case LED_GREEN:
            bitSet(leds, green);
            bitClear(leds, blue);
            bitClear(leds, red);
            break;
        case LED_BLUE:
            bitClear(leds, green);
            bitSet(leds, blue);
            bitClear(leds, red);
            break;
        case LED_RED:
            bitClear(leds, green);
            bitClear(leds, blue);
            bitSet(leds, red);
            break;
    }
    updateShiftRegister();
    delay(tDelay);

    return state;
}

void loop()
{
    if (digitalRead(BUTTON_1) == LOW)
    {
        Serial.println("appuis btn 1");
        Serial.print("state 1 : ");
        Serial.println(state1);
        state1 = updateState(state1, GREEN_1, BLUE_1, RED_1);
        delay(200);
        Serial.print("state 1 : ");
        Serial.println(state1);
    }
    if (digitalRead(BUTTON_2) == LOW)
    {

        Serial.println("appuis btn 2");
        state2 = updateState(state2, GREEN_2, BLUE_2, RED_2);
        delay(200);
        Serial.print("state 2 : ");
        Serial.println(state2);
    }
    if (digitalRead(BUTTON_3) == LOW)
    {

        Serial.println("appuis btn 3");
        state3 = updateState(state3, GREEN_3, BLUE_3, RED_3);
        delay(200);
        Serial.print("state 3 : ");
        Serial.println(state3);
    }

    if (state1 == LED_RED && state2 == LED_RED && state3 == LED_RED) {
        if (!win) {
            Serial.println("Bravo c'est déverrouillé");
            motor.write(180);
            win = true;
        }
    } else if (win) {
        motor.write(0);
        win = false;
    }
}

