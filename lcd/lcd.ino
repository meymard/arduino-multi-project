//www.elegoo.com
//2016.12.9

#include <LiquidCrystal.h>
#include <Servo.h>

#define UP_PIN 3
#define DOWN_PIN 5
#define LEFT_PIN 4
#define RIGHT_PIN 2
#define CHECK_PIN 13

#define SERVO_PIN 6


Servo motor;

const int max_pos = 16;

const char code[max_pos] = {'m', 'a', 'r', 'c'};

int position = 0;
int lettreMvt = 0;
char selected[max_pos];

//                BS  E  D4 D5  D6 D7
LiquidCrystal lcd(7, 8, 9, 10, 11, 12);
void setup()
{
    pinMode(UP_PIN, INPUT_PULLUP);
    pinMode(DOWN_PIN, INPUT_PULLUP);
    pinMode(LEFT_PIN, INPUT_PULLUP);
    pinMode(RIGHT_PIN, INPUT_PULLUP);
    pinMode(CHECK_PIN, INPUT_PULLUP);

    motor.attach(SERVO_PIN);

    motor.write(0);

    Serial.begin(9600);

    lcd.begin(16, 2);

    clearLines();
}

void loop()
{
    getButtonsInfo();
    showText();

    if (digitalRead(CHECK_PIN) == LOW) {
        bool is_ok = validateCode();
        clearLines();

        if (is_ok) {
            lcd.setCursor(0, 0);
            lcd.print("Bravo !");
            lcd.setCursor(0, 1);
            lcd.print("Cool !");
            motor.write(180);
            delay(10000);
            motor.write(0);
        } else {
            lcd.setCursor(0, 0);
            lcd.print("Dommage !");
            lcd.setCursor(0, 1);
            lcd.print("Tu peux reessayer.");
            motor.write(0);
            delay(5000);
        }

        for (int i = 0; i < max_pos; i++) {
            selected[i] = NULL;
        }
        position = 0;

        clearLines();
    }

    delay(100);
}

bool validateCode() {
    for (int i = 0; i < max_pos; i++) {
        if (selected[i] != code[i]) {
            return false;
        }
    }

    return true;
}

void showText() {
    if (position-1 >= 0) {
        lcd.setCursor(position-1, 1);
        lcd.print(" ");
    }
    if (position+1 < max_pos) {
        lcd.setCursor(position+1, 1);
        lcd.print(" ");
    }
    lcd.setCursor(position, 1);
    lcd.print("^");

    char lettre = getChar(position, lettreMvt);
    lcd.setCursor(position, 0);
    if (lettre == NULL) {
        lcd.print(" ");
    } else {
        lcd.print(lettre);
    }
}

char getChar(int pos, int mvt) {
    char previous = selected[pos];
    char newVal = previous;
    if (mvt == -1) {
        if (previous == 'a') {
            newVal = NULL;
        } else if (previous == NULL) {
            newVal = 'z';
        } else {
            newVal = (int)previous - 1;
        }
    } else if (mvt == 1) {
        if (previous == 'z') {
            newVal = NULL;
        } else if (previous == NULL) {
            newVal = 'a';
        } else {
            newVal = (int)previous + 1;
        }
    }
    selected[pos] = newVal;

    if (newVal != previous) {
        Serial.print("Lettre ");
        Serial.print(newVal);
        Serial.print(", mvt ");
        Serial.println(mvt);
    }

    return newVal;
}

void clearLine(int l) {
    lcd.setCursor(0, l);
    for (int j = 0;j < max_pos;j++) {
        lcd.print(" ");
    }
}

void clearLines() {
    lcd.setCursor(0, 0);
    clearLine(0);
    clearLine(1);
}

void getButtonsInfo() {
    int oldPos = position;

    if (digitalRead(RIGHT_PIN) == LOW) {
        position -= 1;
        if (position < 0) {
            position = 0;
        }
    } else if (digitalRead(LEFT_PIN) == LOW) {
        position += 1;
        if (position >= max_pos) {
            position = max_pos - 1;
        }
    }
    if (oldPos != position) {
        Serial.print("Position  ");
        Serial.println(position);
    }

    int oldLettreMvt = lettreMvt;

    if (digitalRead(DOWN_PIN) == LOW) {
        lettreMvt = -1;
    } else if (digitalRead(UP_PIN) == LOW) {
        lettreMvt = 1;
    } else {
        lettreMvt = 0;
    }
    if (oldLettreMvt != lettreMvt) {
        Serial.print("Lettre mvt  ");
        Serial.println(lettreMvt);
    }
}
